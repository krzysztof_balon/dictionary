<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/css" href="freedict-dictionary.css"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <title>${from}-${to} Joined Dictionary</title>
         </titleStmt>
      </fileDesc>
   </teiHeader>
   <text>
      <body>
        <#list translations?keys as phrase>
            <entry>
                <form>
                    <orth>${phrase}</orth>
                </form>
                <#list translations[phrase] as translation>
                <sense>
                    <cit type="trans">
                        <quote>${translation}</quote>
                    </cit>
                </sense>
                </#list>
            </entry>
        </#list>
      </body>
   </text>
</TEI>