package dictionary.parser;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class TeiDictionaryParseHandler extends DefaultHandler {
    @Getter
    private final Map<String, Set<String>> translations = new HashMap<>();
    private Stack<Entry> currentEntries = new Stack<>();
    private boolean isEntry;
    private boolean isForm;
    private boolean isPhrase;
    private int senseDepth = 0;
    private boolean isTranslation;
    private boolean isIdiom;
    private boolean isIdiomPhrase;
    private boolean isTranslationContent;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if ("entry".equalsIgnoreCase(qName)) {
            isEntry = true;
        }
        if (isEntry && "form".equalsIgnoreCase(qName)) {
            isForm = true;
        }
        if (isForm && "orth".equalsIgnoreCase(qName)) {
            isPhrase = true;
        }
        if (isEntry && "sense".equalsIgnoreCase(qName)) {
            senseDepth++;
        }
        if (senseDepth > 0 && "cit".equalsIgnoreCase(qName)) {
            if ("trans".equalsIgnoreCase(attributes.getValue("type"))) {
                isTranslation = true;
            } else if ("idiom".equalsIgnoreCase(attributes.getValue("type"))) {
                isIdiom = true;
            }
        }
        if ("quote".equalsIgnoreCase(qName)) {
            if (isTranslation) {
                isTranslationContent = true;
            } else if (isIdiom) {
                isIdiomPhrase = true;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (isEntry && "entry".equalsIgnoreCase(qName)) {
            isEntry = false;
            while (!currentEntries.isEmpty()) {
                Entry currentEntry = currentEntries.pop();
                translations.put(currentEntry.phrase, currentEntry.translations);
            }
        }
        if (isForm && "form".equalsIgnoreCase(qName)) {
            isForm = false;
        }
        if (isPhrase && "orth".equalsIgnoreCase(qName)) {
            isPhrase = false;
        }
        if ("sense".equalsIgnoreCase(qName)) {
            senseDepth--;
        }
        if ("cit".equalsIgnoreCase(qName)) {
            if (isTranslation) {
                isTranslation = false;
            } else if (isIdiom){
                isIdiom = false;
                Entry idiomEntry = currentEntries.pop();
                translations.put(idiomEntry.phrase, idiomEntry.translations);
            }
        }
        if ("quote".equalsIgnoreCase(qName)) {
            if (isTranslation) {
                isTranslationContent = false;
            } else if (isIdiom) {
                isIdiomPhrase = false;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (isPhrase) {
            currentEntries.add(new Entry(new String(ch, start, length)));
        }
        if (isIdiomPhrase) {
            currentEntries.add(new Entry(new String(ch, start, length)));
        }
        if (isTranslationContent) {
            currentEntries.peek().addTranslation(new String(ch, start, length));
        }
    }

    @ToString
    @RequiredArgsConstructor
    static class Entry {
        private final String phrase;
        private final Set<String> translations = new LinkedHashSet<>();

        void addTranslation(String translation) {
            translations.add(translation);
        }
    }
}
