package dictionary;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class JoinedDictionary {
    private final Map<String, Set<String>> translations;

    public static JoinedDictionary join(Map<String, Set<String>> leftDictionary, Map<String, Set<String>> rightDictionary) {
        Map<String, Set<String>> joinedTranslations = new TreeMap<>();
        for (Map.Entry<String, Set<String>> leftEntry : leftDictionary.entrySet()) {
            Set<String> leftWordJoinedTranslations = new TreeSet<>();
            for (String leftTranslation : leftEntry.getValue()) {
                Set<String> rightTranslations = rightDictionary.getOrDefault(leftTranslation, Collections.emptySet());
                leftWordJoinedTranslations.addAll(rightTranslations);
            }
            if (!leftWordJoinedTranslations.isEmpty()) {
                joinedTranslations.put(leftEntry.getKey(), leftWordJoinedTranslations);
            }
        }
        return new JoinedDictionary(joinedTranslations);
    }
}
