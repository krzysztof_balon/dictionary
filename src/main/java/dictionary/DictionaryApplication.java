package dictionary;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import dictionary.parser.TeiDictionaryParseHandler;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.Set;

@Slf4j
public class DictionaryApplication {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TemplateException {
        Preconditions.checkArgument(args.length >= 4, "App needs 4 arguments");
        String leftDictPath = args[0];
        String rightDictPath = args[1];
        String dictFromLang = args[2];
        String dictToLang = args[3];

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setXIncludeAware(true);
        saxParserFactory.setNamespaceAware(true);
        saxParserFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        saxParserFactory.setFeature("http://apache.org/xml/features/xinclude", true);
        saxParserFactory.setFeature("http://apache.org/xml/features/xinclude/fixup-base-uris", true);

        SAXParser saxParser = saxParserFactory.newSAXParser();

        TeiDictionaryParseHandler spaEngDictionaryParser = new TeiDictionaryParseHandler();
        saxParser.parse(new File(leftDictPath), spaEngDictionaryParser);

        Map<String, Set<String>> spaEngTranslations = spaEngDictionaryParser.getTranslations();
        log.info("Left dictionary translations: {}", spaEngTranslations);

        TeiDictionaryParseHandler engPolDictionaryParser = new TeiDictionaryParseHandler();
        saxParser.parse(new File(rightDictPath), engPolDictionaryParser);

        Map<String, Set<String>> engPolTranslations = engPolDictionaryParser.getTranslations();
        log.info("Right dictionary translations: {}", engPolTranslations);

        JoinedDictionary joinedDictionary = JoinedDictionary.join(spaEngTranslations, engPolTranslations);
        log.info("Joined dictionary translations: {}", joinedDictionary.getTranslations());

        Map<String, Object> dataModel = ImmutableMap.of("from", dictFromLang, "to", dictToLang,
                "translations", joinedDictionary.getTranslations());

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setClassForTemplateLoading(DictionaryApplication.class, "/");
        cfg.setDefaultEncoding(Charsets.UTF_8.name());
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        Template template = cfg.getTemplate("joined_dictionary_template.ftl");

        try (Writer fileWriter = new FileWriter(new File(dictFromLang + "-" + dictToLang + "-joined.tei"))) {
            template.process(dataModel, fileWriter);
        }
    }
}
